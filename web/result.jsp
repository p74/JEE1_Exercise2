<%--
  Created by IntelliJ IDEA.
  User: pouyan
  Date: 3/1/18
  Time: 12:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
    <%
        double num1 = Double.parseDouble(request.getParameter("num1"));
        double num2 = Double.parseDouble(request.getParameter("num2"));
        String op = request.getParameter("op");
        String opchar = null;
        double result = 0;

        switch (op) {
            case("a"):
                result = num1 + num2;
                opchar = "+";
                break;
            case("s"):
                result = num1 - num2;
                opchar = "-";
                break;
            case("m"):
                result = num1 * num2;
                opchar = "×";
                break;
            case("d"):
                result = num1 / num2;
                opchar = "÷";
                break;
        }
    %>
</head>
<body>
    <h1><%=num1%> <%=opchar%> <%=num2%> = <%=result%></h1>
</body>
</html>
